// import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { RegisterFormComponent } from './components/register-form/register-form.component';
import { CourseDetailsComponent } from './components/course-details/course-details.component';
import { LoaderComponent } from './components/loader/loader.component';
import { JwPaginationComponent } from './components/jw-pagination/jw-pagination.component';
import { StartComponent } from './components/start/start.component';
import { BannerComponent } from './components/banner/banner.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { CoursesListComponent } from './components/courses-list/courses-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RegisterSuccessComponent } from './components/register-success/register-success.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    BannerComponent,
    NavigationComponent,
    StartComponent,
    CoursesListComponent,
    JwPaginationComponent,
    LoaderComponent,
    CourseDetailsComponent,
    RegisterFormComponent,
    RegisterSuccessComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
    // FontAwesomeModule

  ],
  providers: [],

  bootstrap: [AppComponent]
})
export class AppModule { }
