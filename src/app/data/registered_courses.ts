export interface RegisteredCourseData {
        name: string;
        surrname: string;
        email: string;
        date: string;
        validation_data: boolean;
        id_course: number;
}

