export interface Navigation{
  id: number;
  label: string;
  desc: string;
  route: string;
  route_level: number;
}


export const NAVIGATION: Navigation[] = [
  {
    id: 1,
    label: 'Start',
    desc: '',
    route: '',
    route_level: 0
  },
  {
    id: 2,
    label: 'Lista kursów',
    desc: '',
    route: '/courses',
    route_level: 1
  }
  // {
  //   id: 3,
  //   label: 'Rejestracja na kurs',
  //   desc: '',
  //   route: '',
  //   route_level: 1
  // }
];


