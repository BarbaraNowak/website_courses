import { RegisterSuccessComponent } from './components/register-success/register-success.component';
import { CourseDetailsComponent } from './components/course-details/course-details.component';
import { CoursesListComponent } from './components/courses-list/courses-list.component';
import { StartComponent } from './components/start/start.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', component: StartComponent },
  { path: 'courses', component: CoursesListComponent },
  { path: 'course-details/:id', component: CourseDetailsComponent},
  { path: 'course-details/:id/register-success', component: RegisterSuccessComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
