
import { Component, Input, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/services/loader.service';


@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements OnInit {
    loaderSize = [1]; // loader-icon, spinner-border
    // loaderSize = [1, 2, 3, 4]; //(dla spinner-grow)

    loaderType =  'spinner-border';
    lang: any;
    // loaderType =  'spinner-grow';

    loader = {
      status: false,
      content: 'Trwa wczytywanie danych...'
    };
    // @Input() content: string;

    constructor(private loaderService: LoaderService) {}

    ngOnInit(): void {

        this.loaderService.load$.subscribe(res => {
            this.loader = res;
        });
    }
}
