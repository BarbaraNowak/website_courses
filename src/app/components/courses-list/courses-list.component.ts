import { Subscription } from 'rxjs';
import { StorageService } from './../../services/storage.service';
import { LoaderService } from 'src/app/services/loader.service';
import { COURSES, COURSE_CATEGORIES, Course, CourseCategory } from './../../data/courses';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.scss']
})
export class CoursesListComponent implements OnInit, OnDestroy {

  coursesList: Course[] = [];
  coursesCategories: CourseCategory[] = [];

  subscriptions: Subscription[] = [];

  paginationItems: any[] = [];
  pageOfItems: any[] = [];
  paginationConfig: any = {
    pageSize: 12,
    initialPage: 1
  };

  // = = =  Loader  = = =
  loader: any = {status: false, content: ''};

  storage: any = null;

  form = this.fb.group({
    id_category: this.fb.control(0),
    search_word: this.fb.control('')
  });

  constructor(
    private loaderService: LoaderService,
    private fb: FormBuilder,
    private storageService: StorageService
  ) { }

  ngOnInit(): void {

    this.setLoader(true, 'Trwa wczytywanie danych...');

    this.subscriptions.push(
      this.storageService.storage$.subscribe(d => {
        this.storage = d;
      })
    );

    this.subscriptions.push(
      this.storageService.apiData$.subscribe( d => {
        this.coursesList = d.courses;
        this.coursesCategories = d.courses_categories;
      })
    );

    this.sortCourses(this.coursesList, 1);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewChecked(){
    this.setLoader(false)
  }


  sortCourses(arr: any[], type: number){
    if (arr){
      if (arr.length > 0){
        const sortArr = arr.sort(this.getSortOrder('label', type));
        this.paginationItems = sortArr;
      }else{
        this.paginationItems = arr;
      }
    }else{
      this.paginationItems = [];
    }
    // this.setLoader(false);
  }

  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }

  getSortOrder(prop: string, direction: number) {
    if (direction == 1) //asc
    {
      return (a, b) => {
        if (a[prop] > b[prop]) {
            return 1;
        } else if (a[prop] < b[prop]) {
            return -1;
        }
        return 0;
       };

    }else if (direction == 2){ //desc
      return (a, b) => {
        if (a[prop] < b[prop]) {
            return 1;
        } else if (a[prop] > b[prop]) {
            return -1;
        }
        return 0;
       };
    }
  }

  setLoader(type: boolean, text?: string){
    text = type == false ? '' : text;
    this.loader = {status: type, content: text};
    this.loaderService.load = this.loader;
  }

  setFormControlValue(param:string,value:any){
    this.form.get(param).setValue(value);
  }

  get formValues(){
    return this.form.value;
  }

  getFormControlValue(param:string){
    return  this.form.get(param).value;
  }

  refreshList(){
    let idCategory = this.getFormControlValue('id_category');
    let searchWord = this.getFormControlValue('search_word').toLowerCase();
    let resultArr: any[] = [];

    let arr: any[] = [];
    if (idCategory > 0 ){
      arr = this.coursesList.filter( d => d.id_category == idCategory);
    }else{
      arr = this.coursesList;
    }

    let arr2: any[] = [];
    if (searchWord != ''){
      arr2 = arr.filter(d => (((d.label).toLowerCase()).includes(searchWord)) == true);
    }else{
      arr2 = arr;
    }

    resultArr = arr2;

    // console.log(arr2)
    this.sortCourses(resultArr, 1);
  }

  selectCourse(course: any){
    this.storage.selectedCourse = course;
    this.storageService.setStorage(this.storage);
  }

  ngOnDestroy(){
    this.subscriptions.forEach(e => !e.closed ? e.unsubscribe() : null);
  }


}
