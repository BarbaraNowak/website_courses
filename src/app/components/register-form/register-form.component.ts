import { CourseCategory, Course } from './../../data/courses';
import { CourseRegisterService } from './../../services/course-register.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { StorageService } from './../../services/storage.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, HostListener, OnInit, OnDestroy, Input } from '@angular/core';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit, OnDestroy {

  submitted = false;
  selectedCourseId: any = null;
  selectedCourse: any = null;

  form = this.fb.group({
    name: this.fb.control('', [Validators.required]),
    surrname: this.fb.control(''),
    email: this.fb.control('', [Validators.required, Validators.email]),
    date: this.fb.control('', [Validators.required])
  });

  formSteps:any[] = [
    {id: 1, status: false},
    {id: 2, status: false}
  ];

  activeFormStepIndex = 0;

  subscriptions: Subscription[] = [];
  storage: any = null;

  coursesList: Course [] = [];
  coursesCategories: CourseCategory[] = [];

  dateToday = new Date();

  constructor(
    private fb: FormBuilder,
    private storageService: StorageService,
    private route: ActivatedRoute,
    private router: Router,
    private courseRegisterService: CourseRegisterService
  ) { }

  ngOnInit() {

    this.subscriptions.push(
      this.storageService.storage$.subscribe(d => {
        this.storage = d;
      })
    );


  }

  setFormControlValue(param: string, value: any){
    this.form.get(param).setValue(value);
  }

  get formValues(){
    return this.form.value;
  }

  getFormControlValue(param: string){
    return  this.form.get(param).value;
  }

  get formControls() {
    return this.form.controls;
  }

  backToStep(step: number){
    this.formSteps[step - 1].status = false;
    this.activeFormStepIndex = step - 1;
    this.setStorage('set-storage');
  }

  @HostListener('submit')
  onSubmit(step: number){

    // console.log(this.form,step)
    this.submitted = true;

    switch(step){
      case 1:
        if(this.checkValidationForm(step, 'all-params')){
          // przejście do kroku 2
          this.formSteps[step-1].status = true;
          this.activeFormStepIndex = step;
          this.setStorage('set-storage');
        }
      break;
      case 2:
        if(this.form.status == 'VALID'){
          // przejście do strony z komponentem potwierdzajacym wyslanie zgloszenia
          this.setStorage('set-storage');
          let obj = {
            name: this.getFormControlValue('name'),
            surrname: this.getFormControlValue('surrname'),
            email: this.getFormControlValue('email'),
            date: this.getFormControlValue('date'),
            validation_data: this.form.status == 'VALID' ? true : false,
            id_course: this.storage.selectedCourse.id,
            activeRegisterStepIndex: this.activeFormStepIndex
          }
          this.courseRegisterService.addData(obj);
          this.gotoPage('register-success')
        }
      break;
    }



  }

  setStorage(type: string){

    let item = {
      selectedCourse: this.storage.selectedCourse,
      registerFormData: {
        name: this.getFormControlValue('name'),
        surrname: this.getFormControlValue('surrname'),
        email: this.getFormControlValue('email'),
        date: this.getFormControlValue('date'),
        validation_data: this.form.status == 'VALID' ? true : false,
        id_course: this.storage.selectedCourse ? this.storage.selectedCourse.id : 0,
        activeRegisterStepIndex: this.activeFormStepIndex
      }
    };

    switch(type){
      case 'set-storage':
        this.storageService.setStorage(item);
        break;
    }


  }

  checkValidationForm(step: number, param: string, type?: string){
    let retValue = true; //true-invalid, false-valid
    switch(step){
      case 1:
          switch(param){
            case 'name':
                retValue = (this.formControls.name.touched || this.submitted) && this.formControls.name.errors?.required;
            break;

            case 'email':
              switch(type){
                case 'required':
                  retValue = (this.formControls.email.touched || this.submitted)  && (this.formControls.email.errors?.required);
                break;
                case 'email':
                  retValue = this.formControls.email.touched && this.formControls.email.errors?.email && this.submitted;
                break;
              }

            break;

            case 'all-params':
              retValue = !(this.formControls.name.status == 'VALID' && this.formControls.email.status == 'VALID' ? false : true);
          break;
          }
      break;
      case 2:
        switch(param){
          case 'date':
            switch(type){
              case 'required':
                retValue = (this.formControls.date.touched || this.submitted) && this.formControls.date.errors?.required;
              break;
            }
          break;
        }
      break;
    }

    return retValue;
  }

  ngOnDestroy(){
    this.subscriptions.forEach(e => !e.closed ? e.unsubscribe() : null);
  }

  gotoPage(label: string) {
    switch (label){
      case 'register-success':
        this.router.navigate(['/course-details/' + this.selectedCourseId + '/register-success']);
        break;
    }
  }


}
