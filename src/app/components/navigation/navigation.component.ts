import { NAVIGATION } from './../../data/navigation';
import { Component, OnInit } from '@angular/core';
// import { faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  navigationList = NAVIGATION;
  // iconAngleDoubleRight = faAngleDoubleRight;

  constructor() { }

  ngOnInit(): void {
  }

}
