import { StorageService } from './../../services/storage.service';
import { COURSES, COURSE_CATEGORIES, Course, CourseCategory } from './../../data/courses';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.scss']
})
export class CourseDetailsComponent implements OnInit, OnDestroy {

  subscriptions: Subscription[] = [];

  selectedCourse: any = null;

  coursesList: Course[] = [];
  coursesCategories: CourseCategory[] = [];
  storage: any = null;

  constructor(
    private route: ActivatedRoute,
    private storageService: StorageService
  ) { }

  ngOnInit() {


    this.subscriptions.push(
      this.storageService.apiData$.subscribe( d => {
        this.coursesList = d.courses;
        this.coursesCategories = d.courses_categories;

      })
    );

    this.subscriptions.push(
      this.storageService.storage$.subscribe(d => {
        this.storage = d;
        this.getSelectedCourseFromUrl();
      })
    );

  }

  getSelectedCourseFromUrl(){
    let self = this;
    this.subscriptions.push(
      this.route.params.subscribe( data => {
        let id = +data.id;
        self.selectedCourse = COURSES.filter( c => c.id == id)[0];
        self.storage.selectedCourse = self.selectedCourse;
      })
     );
  }

  ngOnDestroy(){
    this.subscriptions.forEach(e => !e.closed ? e.unsubscribe() : null);
  }

  getCategoryName(){
    return this.coursesCategories.filter( c => c.id == this.selectedCourse.id_category)[0].label;
  }

}
