import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { StorageService } from './../../services/storage.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-register-success',
  templateUrl: './register-success.component.html',
  styleUrls: ['./register-success.component.scss']
})
export class RegisterSuccessComponent implements OnInit, OnDestroy {

  subscriptions: Subscription[] = [];
  storage: any = null;

  constructor(
    private storageService: StorageService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.subscriptions.push(
      this.storageService.storage$.subscribe(d => {
        this.storage = d;
        // console.log(d)
        if(this.storage.selectedCourse == null){
          this.gotoPage('courses')
        }
      })
    );
  }

  ngOnDestroy(){
    this.subscriptions.forEach(e => !e.closed ? e.unsubscribe() : null);
  }

  gotoPage(label: string) {
    switch (label){
      case 'courses':
        this.router.navigate(['/courses']);
        break;
    }
  }

}
