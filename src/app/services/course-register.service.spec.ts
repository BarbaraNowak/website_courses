import { TestBed } from '@angular/core/testing';

import { CourseRegisterService } from './course-register.service';

describe('CourseRegisterService', () => {
  let service: CourseRegisterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CourseRegisterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
