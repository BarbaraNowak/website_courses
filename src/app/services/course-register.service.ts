import { RegisteredCourseData } from './../data/registered_courses';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CourseRegisterService {

  private registeredCoursesList = new BehaviorSubject<RegisteredCourseData[]>([]);
  registerCoursesList$ = this.registeredCoursesList.asObservable();

  setRegisteredCoursesList(value: any){
    this.registeredCoursesList.next(value);
  }

  constructor() { }

  addData(item: RegisteredCourseData): void {
    this.registeredCoursesList.next(this.registeredCoursesList.getValue().concat([item]));
    // console.log(this.registeredCoursesList)
  }
}


