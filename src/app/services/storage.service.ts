import { ActivatedRoute } from '@angular/router';
import { COURSES, COURSE_CATEGORIES } from './../data/courses';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

private storage = new BehaviorSubject<any>(this.defaultStorage()); //use in service
storage$ = this.storage.asObservable(); //use in components

private apiData = new BehaviorSubject<any>({
  courses: COURSES,
  courses_categories: COURSE_CATEGORIES
});
apiData$ = this.apiData.asObservable();

constructor(
  private route: ActivatedRoute
) { }


setStorage(value: any){
  // console.log(value)
  this.storage.next(value);
}

resetStorage(){
  this.storage.next(this.defaultStorage());
}


defaultStorage(){
  return {
    selectedCourse: null,
    registerFormData: {
      name: '',
      surrname: '',
      email: '',
      date: '',
      validation_data: false,
      activeRegisterStepIndex: 0,
      id_course: 0
    }
  };
}




}
