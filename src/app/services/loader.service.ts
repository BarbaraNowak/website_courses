import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  constructor() { }

  loader = {
    status: false,
    content: 'Proszę czekać...'
  };

  private loadSource: BehaviorSubject<any> = new BehaviorSubject(this.loader);
  load$ = this.loadSource.asObservable();

  get load(): any {
    return this.loadSource.value;
  }

  set load(data: any) {
    this.loadSource.next(data);
  }
}
